package id.mobile.ebook.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.mobile.ebook.R
import id.mobile.ebook.modules.Book
import kotlinx.android.synthetic.main.list_book.view.*

class BookRecycler(val context: Context, val itemList: List<Book>): RecyclerView.Adapter<BookRecycler.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.list_book, parent, false))
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemList.get(position)
        val options = RequestOptions()
        options.placeholder(R.drawable.img_default)
        options.error(R.drawable.img_default)
        Glide.with(context).load(item.info.thumbnail!!.small).apply(options).into(holder.imageBook)
        holder.tvBookTitle.setText(item.info.title)
        try {
            holder.tvBookAuthor.setText("Author : " + item.info.authors!!.joinToString(","))
        } catch (e: Exception){
            holder.tvBookAuthor.setText("Author : ")
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val imageBook = view.image_book
        val tvBookTitle = view.tv_book_title
        val tvBookAuthor = view.tv_book_author
        val rattingBook = view.ratting_book
    }


}