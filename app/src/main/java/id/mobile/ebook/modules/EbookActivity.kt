package id.mobile.ebook.modules

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class EbookActivity: AppCompatActivity(){
    lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        afterCreate()
    }

    fun progressDialogShow(title: String, message: String) {
        progressDialog = ProgressDialog.show(this, title, message)
    }

    fun progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss()
    }

    abstract fun afterCreate()
}