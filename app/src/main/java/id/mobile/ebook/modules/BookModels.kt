package id.mobile.ebook.modules

import com.google.gson.annotations.SerializedName

data class BookModels(
    @SerializedName("items")
    val books: List<Book>
)

data class Book(
    @SerializedName("id")
    var id: String? = null,

    @SerializedName("volumeInfo")
    var info: Info
)

data class Info(
    @SerializedName("title")
    var title: String? = null,

    @SerializedName("subtitle")
    var subtitle: String? = null,

    @SerializedName("authors")
    var authors: Array<String>? = null,

    @SerializedName("imageLinks")
    var thumbnail: Thumnail? = null
)

data class Thumnail(
    @SerializedName("smallThumbnail")
    var small: String? = null,

    @SerializedName("thumbnail")
    var real: String? = null
)