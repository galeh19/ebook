package id.mobile.ebook

import android.content.Context
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.mobile.ebook.modules.Book
import id.mobile.ebook.modules.BookModels
import id.mobile.ebook.modules.EbookActivity
import id.mobile.ebook.modules.HttpRequest
import id.mobile.ebook.recycler.BookRecycler
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : EbookActivity() {
    lateinit var bookRecycler: BookRecycler
    lateinit var itemList: ArrayList<Book>
    override fun afterCreate() {
        setContentView(R.layout.activity_main)

        itemList = ArrayList()
        bookRecycler = BookRecycler(this, itemList)

        recycler_book.adapter = bookRecycler
        recycler_book.layoutManager = LinearLayoutManager(this)

        et_search.setOnEditorActionListener {
                v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                loadData()
                true
            }
            false
        }
        rel_search.setOnClickListener { loadData() }
    }

    fun loadData(){
        val input: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        input.hideSoftInputFromWindow(et_search.windowToken, 0)
        progressDialogShow("Waiting", "Loading data...")
        itemList.clear()
        bookRecycler.notifyDataSetChanged()
        HttpRequest().services.getBooks(et_search.text.toString()).enqueue(object: Callback<BookModels> {
            override fun onFailure(call: Call<BookModels>, t: Throwable) {
                progressDialogDismiss()
                itemList.clear()
                bookRecycler.notifyDataSetChanged()
            }

            override fun onResponse(call: Call<BookModels>, response: Response<BookModels>) {
                progressDialogDismiss()
                if(response.code() == 200) {
                    itemList.addAll(response.body()?.books!!)
                    bookRecycler.notifyDataSetChanged()
                }
            }
        })
    }
}
